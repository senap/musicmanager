package ru.musicandaudio.audiovksaverplus;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.badoo.mobile.util.WeakHandler;
import com.franmontiel.persistentcookiejar.persistence.SharedPrefsCookiePersistor;
import com.yandex.metrica.YandexMetrica;

import java.io.IOException;
import java.util.regex.Matcher;

import ru.musicandaudio.audiovksaverplus.service.ManagerConnectService;
import ru.musicandaudio.audiovksaverplus.vk.VkMainActivity;
import ru.musicandaudio.audiovksaverplus.vk.model.User;
import ru.musicandaudio.audiovksaverplus.vk.util.HttpWrapperRequest;
import ru.musicandaudio.audiovksaverplus.vk.util.PatternRegex;


public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private ProgressBar progressBar, progressBar2;
    private EditText login, passwd;
    private WeakHandler handler;
    private SharedPreferences sharedPref;
    private TextView textViewHello;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_main);
        handler = new WeakHandler();
        sharedPref = getPreferences(Context.MODE_PRIVATE);

//          for auth
        login = findViewById(R.id.searchText);
        passwd = findViewById(R.id.passwd);
        progressBar = findViewById(R.id.progressBar);
//          for welcome
        textViewHello = findViewById(R.id.textViewHello);
        progressBar2 = findViewById(R.id.progressBar2);


        String userId = sharedPref.getString("id", null);
        if(userId != null)
            showWelcome();
        else
            showAuth();

        setClickListeners();
        ManagerConnectService.manager(this);

    }

    @Override
    public void onClick(View view){
        switch(view.getId()) {
            case R.id.enter_vk:
                YandexMetrica.reportEvent("auth VK: Click on button login");

                String loginText = login.getText().toString().trim();
                String passwdText = passwd.getText().toString().trim();

                if(loginText.equals("")){
                    login.setError("Введите логин или e-mail");
                    break;
                }

                if(passwdText.equals("")){
                    passwd.setError("Введите пароль");
                    break;
                }

                Log.d("UI_LOG", "CLICK ON VK BUTTON");

               /* SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString("password", passwdText);
                editor.putString("login", loginText);
                editor.commit();*/

                authInVk(loginText, passwdText);
                //changeActivity(VkMainActivity.class);
                break;
            case R.id.developer:
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("market://search?q=pub:olga4ever"));
                startActivity(intent);
                break;
            case R.id.rights_holders:
                showRightDialog();
                break;

            case R.id.activity_main:
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                break;

            case R.id.continue_vk:
                changeActivity(VkMainActivity.class);
                break;
            case R.id.change_account_vk:
                SharedPrefsCookiePersistor loadCookie =  new SharedPrefsCookiePersistor(this);
                loadCookie.clear();

                showAuth();
                break;
        }
    }

    private void showWelcome(){
        final HttpWrapperRequest httpWrapperRequest = new HttpWrapperRequest(this);
        textViewHello.setVisibility(View.INVISIBLE);
        progressBar2.setVisibility(View.VISIBLE);

        findViewById(R.id.auth_main).setVisibility(View.GONE);
        findViewById(R.id.welcome_main).setVisibility(View.VISIBLE);

        findViewById(R.id.continue_vk).setEnabled(false);
        try {
            httpWrapperRequest.isAuth(new HttpWrapperRequest.VKActions() {
                @Override
                public void onSuccess(String data) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {

                            String userName = sharedPref.getString("name", null);
                            String userId = sharedPref.getString("id", null);
                            textViewHello.setVisibility(View.VISIBLE);
                            progressBar2.setVisibility(View.INVISIBLE);
                            findViewById(R.id.continue_vk).setEnabled(true);

                            textViewHello.setText("Привет " + userName + "!");

                            User.ID = userId;
                            User.NAME = userName;
                        }
                    });
                }

                @Override
                public void onFail(String data) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            showAuth();
                        }
                    });
                }

                @Override
                public void onError(Exception e) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            showAuth();
                        }
                    });
                }
            });
        }catch (IOException e){
            YandexMetrica.reportError("MainActivty showWelcome():", e);
            showAuth();
        }

    }

    private void showAuth(){
        findViewById(R.id.welcome_main).setVisibility(View.GONE);
        findViewById(R.id.auth_main).setVisibility(View.VISIBLE);
        //setSavedEditText();
    }


    private void setClickListeners(){
        findViewById(R.id.enter_vk).setOnClickListener(this);
        findViewById(R.id.activity_main).setOnClickListener(this);
        findViewById(R.id.developer).setOnClickListener(this);
        findViewById(R.id.rights_holders).setOnClickListener(this);
        findViewById(R.id.continue_vk).setOnClickListener(this);
        findViewById(R.id.change_account_vk).setOnClickListener(this);

        //findViewById(R.id.enter_ok).setOnClickListener(this);
    }

   /* private void setSavedEditText(){
        SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
        String password = sharedPref.getString("password", null);
        String login = sharedPref.getString("login", null);

        if(password != null)
            this.passwd.setText(password);
        if(login != null)
            this.login.setText(login);
    }*/

    private void changeActivity(Class activity){
        Intent i = new Intent(this, activity);
        startActivity(i);
        this.finish();
    }

    private void showRightDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Для правообладателей")
                .setMessage(R.string.right)
                .setCancelable(false)
                .setNegativeButton("ОК",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void showMessage(String msg){
        progressBar.setVisibility(View.INVISIBLE);
        findViewById(R.id.enter_vk).setVisibility(View.VISIBLE);

        View activityVkMain = findViewById(R.id.activity_main);
        Snackbar.make(activityVkMain, msg, Snackbar.LENGTH_SHORT).show();
    }

    private void authInVk(String login, String password) {
        progressBar.setVisibility(View.VISIBLE);
        findViewById(R.id.enter_vk).setVisibility(View.INVISIBLE);

       final HttpWrapperRequest httpWrapperRequest = new HttpWrapperRequest(this);
        try {
            httpWrapperRequest.Auth(login, password, new HttpWrapperRequest.VKActions() {

                @Override
                public void onSuccess(String data) {
                    Matcher userName = PatternRegex.exec(PatternRegex.GetUserName, data);
                    if (userName.find()) {
                        User.NAME = userName.group(1);
                    }else {
                        User.NAME = "Друг :)";
                    }

                    YandexMetrica.reportEvent("auth VK: SUCCESS response on auth VK");

                    Matcher muid = PatternRegex.exec(PatternRegex.GetUserID, data);
                    if (muid.find())
                        User.ID = muid.group(1).trim();

                    if(User.ID != null){
                        if(TextUtils.isDigitsOnly(User.ID)){
                            Log.d("HTTP_LOG", User.ID);
                            YandexMetrica.reportEvent("auth VK ID: " + User.ID);
                            /*Log.d("HTTP_LOG", User.NAME);*/
                            SharedPreferences.Editor editor = sharedPref.edit();
                            editor.putString("id", User.ID);
                            editor.putString("name", User.NAME);
                            editor.commit();

                            changeActivity(VkMainActivity.class);
                        }else{
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    showMessage("Не верный формат ID");
                                    YandexMetrica.reportEvent("auth VK: Не верный формат ID");
                                }
                            });
                        }
                    } else {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                showMessage("Не определен ID");
                                YandexMetrica.reportEvent("auth VK: Не определен ID");
                            }
                        });
                    }

                }

                @Override
                public void onFail(String data) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            showMessage("Не верный логин или пароль.");
                            YandexMetrica.reportEvent("auth VK: Не верный логин или пароль.");
                        }
                    });
                }

                @Override
                public void onError(Exception data) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            showMessage("Не верный логин или пароль.");
                            YandexMetrica.reportEvent("auth VK: Не верный логин или пароль.");
                        }
                    });
                }

//                @Override
//                public void authCheckCode(final HttpUrl authKeyCodeURL) {
//                    handler.post(new Runnable() {
//                        @Override
//                        public void run() {
//                            showMessage("Необходимо подтвердить вход.");
//                            YandexMetrica.reportEvent("auth VK: двухуровневая авторизация");
//
//                            final EditText taskEditText = new EditText(MainActivity.this);
//                            AlertDialog dialog = new AlertDialog.Builder(MainActivity.this)
//                                    .setTitle("Введите код из СМС")
//                                    //.setMessage("What do you want to do next?")
//                                    .setView(taskEditText)
//                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                                        @Override
//                                        public void onClick(DialogInterface dialog, int which) {
//                                            String code = String.valueOf(taskEditText.getText());
//                                            try {
//                                                httpWrapperRequest.AuthCheckCode(code, authKeyCodeURL, new HttpWrapperRequest.VKActions() {
//                                                    @Override
//                                                    public void onSuccess(String data) {
//
//                                                    }
//                                                    @Override
//                                                    public void onFail(String data) {
//                                                        handler.post(new Runnable() {
//                                                            @Override
//                                                            public void run() {
//                                                                showMessage("Не верный логин или пароль.");
//                                                                YandexMetrica.reportEvent("auth VK: Не верный логин или пароль.");
//                                                            }
//                                                        });
//                                                    }
//
//                                                    @Override
//                                                    public void onError(Exception data) {
//                                                        handler.post(new Runnable() {
//                                                            @Override
//                                                            public void run() {
//                                                                showMessage("Не верный логин или пароль.");
//                                                                YandexMetrica.reportEvent("auth VK: Не верный логин или пароль.");
//                                                            }
//                                                        });
//                                                    }
//
////                                                    @Override
////                                                    public void authCheckCode(final HttpUrl authKeyCodeURL){
////
////                                                    }
//
//                                                });
//                                            }catch (IOException e){
//                                                YandexMetrica.reportError("auth VK:Проблема с двухкратной авторизацией.", e);
//                                            }
//                                        }
//                                    })
//                                    .setNegativeButton("Cancel", null)
//                                    .create();
//                            dialog.show();
//                        }
//                    });
//                }

            });
        } catch (IOException e) {
            showMessage("Плохое соединение с интернетом.");
            YandexMetrica.reportError("auth VK:Плохое соединение с интернетом.", e);
        }
    }
}
