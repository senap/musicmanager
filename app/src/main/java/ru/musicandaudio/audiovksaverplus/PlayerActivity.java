package ru.musicandaudio.audiovksaverplus;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.Locale;

import jp.wasabeef.blurry.Blurry;
import ru.musicandaudio.audiovksaverplus.vk.Player;
import ru.musicandaudio.audiovksaverplus.vk.model.Audio;

import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;


public class PlayerActivity extends AppCompatActivity implements View.OnClickListener{

    private static final Player playerInstance = Player.getInstance();

    private ImageView mSkipPrev;
    private ImageView mSkipNext;
    private ImageView mPlayPause;
    private TextView mStart;
    private TextView mEnd;
    private SeekBar mSeekbar;
    private TextView mLine1;
    private TextView mLine2;
    private TextView mLine3;
    private ProgressBar mLoading;
    private View mControllers;
    private Drawable mPauseDrawable;
    private Drawable mPlayDrawable;
    private ImageView mBackgroundImage;

    private Player.Description description;

    private Handler mHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_full_player);

        mLine1 = findViewById(R.id.line1);
        mLine2 = findViewById(R.id.line2);
        mBackgroundImage = findViewById(R.id.background_image);

        mPauseDrawable = ContextCompat.getDrawable(this, R.drawable.uamp_ic_pause_white_48dp);
        mPlayDrawable = ContextCompat.getDrawable(this, R.drawable.uamp_ic_play_arrow_white_48dp);
        mPlayPause = (ImageView) findViewById(R.id.play_pause);
        mSkipNext = (ImageView) findViewById(R.id.next);
        mSkipPrev = (ImageView) findViewById(R.id.prev);
        mStart = (TextView) findViewById(R.id.startText);
        mEnd = (TextView) findViewById(R.id.endText);
        mSeekbar = (SeekBar) findViewById(R.id.seekBar1);
        mLine3 = (TextView) findViewById(R.id.line3);
        mLoading = (ProgressBar) findViewById(R.id.progressBar1);
        mControllers = findViewById(R.id.controllers);

        description = playerInstance.getDescription();
        mHandler = new Handler();

        setClickListeners();
        restoreDescription(description);

        RelativeLayout pageView = findViewById(R.id.pageView);
        pageView.setOnTouchListener(new GestureFilter(this));

        setPlayerListener();

    }

    @Override
    public void onClick(View view){
        switch(view.getId()) {
            case R.id.play_pause:
                if(description.getState() == Player.Description.STATE_PLAYING){
                    playerInstance.pause();
                }else if(description.getState() == Player.Description.STATE_PAUSED){
                    playerInstance.play();
                }
                break;
            case R.id.next:
                playerInstance.next();
                break;
            case R.id.prev:
                playerInstance.prev();
                break;
        }
    }


    private void setClickListeners(){
        mPlayPause.setOnClickListener(this);
        mSkipNext.setOnClickListener(this);
        mSkipPrev.setOnClickListener(this);

    }


    private void restoreDescription(Player.Description description){

        switch (description.getState()) {
            case Player.Description.STATE_PLAYING:
                mLoading.setVisibility(INVISIBLE);
                mPlayPause.setVisibility(VISIBLE);
                mPlayPause.setImageDrawable(mPauseDrawable);
                mControllers.setVisibility(VISIBLE);

                mSeekbar.setMax(description.getDuration());
                mSeekbar.setProgress(description.getCurrntDuration());

                break;

            case Player.Description.STATE_PAUSED:
                mLoading.setVisibility(INVISIBLE);
                mPlayPause.setVisibility(VISIBLE);
                mPlayPause.setImageDrawable(mPlayDrawable);
                mControllers.setVisibility(VISIBLE);

                mSeekbar.setMax(description.getDuration());
                mSeekbar.setProgress(description.getCurrntDuration());

                break;
        }

        mLine1.setText(description.getTitle());
        mLine2.setText(description.getArtist());

        setDuration(description.getCurrntDuration(), description.getDuration());

        mSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                mHandler.postDelayed(updateSeekBar, 999);
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                mHandler.removeCallbacks(updateSeekBar);
            }
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    playerInstance.seekTo(progress * 1000);
                }

                setDuration(progress, seekBar.getMax());
            }
        });

        if( description.getImage() != null ){
           // mBackgroundImage.setImageBitmap( BitmapFactory.decodeByteArray(description.getImage(), 0, description.getImage().length));
            //mBackgroundImage.setImageBitmap( description.getImage());
            Blurry.with(getApplicationContext())
                    .radius(5)
                    .from(description.getImage()).into(mBackgroundImage);
        }else {
            Bitmap placeHolderImage = BitmapFactory.decodeResource(getResources(),R.drawable.audio_row_placeholder);
            Blurry.with(getApplicationContext())
                    .radius(5)
                    .from(placeHolderImage).into(mBackgroundImage);
        }

        mHandler.postDelayed(updateSeekBar, 999);
    }

    private void setDuration(int currentDuration, int duraion){
        int s_minutes = (currentDuration % 3600) / 60;
        int s_seconds = currentDuration % 60;

        int end = duraion - currentDuration;

        int e_minutes = (end % 3600) / 60;
        int e_seconds = end % 60;

        mStart.setText(String.format(Locale.ENGLISH, "%02d:%02d", s_minutes, s_seconds));
        mEnd.setText(String.format(Locale.ENGLISH, "-%02d:%02d", e_minutes, e_seconds));
    }

    private void setPlayerListener(){
        playerInstance.setOnceListenerChange(new Player.OnStateListener(){
            @Override
            public void onChange(Audio audio, Player.Description description){
                Log.d("PLAYERACTIVITY ON", "player change state!");
                restoreDescription(description);
            }
            @Override
            public boolean equals(Object object){
                return true;
            }
        });
    }


    private Runnable updateSeekBar = new Runnable(){

        @Override
        public void run()
        {
            if(description.getState() == Player.Description.STATE_PLAYING) {
                mSeekbar.setProgress(description.getCurrntDuration());
            }
            mHandler.postDelayed(updateSeekBar, 999);
        }
    };

    public class GestureFilter implements View.OnTouchListener {

        static final String logTag = "ActivitySwipeDetector";
        private Context activity;
        static final int MIN_DISTANCE = 100;
        private float downX, downY, upX, upY;

        public GestureFilter(Context mainActivity) {
            activity = mainActivity;
        }

        public void onRightToLeftSwipe() {

        }

        public void onLeftToRightSwipe() {

        }

        public void onTopToBottomSwipe() {
            finish();
        }

        public void onBottomToTopSwipe() {

        }

        public boolean onTouch(View v, MotionEvent event) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN: {
                    downX = event.getX();
                    downY = event.getY();
                    return true;
                }
                case MotionEvent.ACTION_UP: {
                    upX = event.getX();
                    upY = event.getY();

                    float deltaX = downX - upX;
                    float deltaY = downY - upY;

                    // swipe horizontal?
                    if (Math.abs(deltaX) > MIN_DISTANCE) {
                        // left or right
                        if (deltaX < 0) {
                            this.onLeftToRightSwipe();
                            return true;
                        }
                        if (deltaX > 0) {
                            this.onRightToLeftSwipe();
                            return true;
                        }
                    } else {
                        Log.i(logTag, "Swipe was only " + Math.abs(deltaX) + " long horizontally, need at least " + MIN_DISTANCE);
                        // return false; // We don't consume the event
                    }

                    // swipe vertical?
                    if (Math.abs(deltaY) > MIN_DISTANCE) {
                        // top or down
                        if (deltaY < 0) {
                            this.onTopToBottomSwipe();
                            return true;
                        }
                        if (deltaY > 0) {
                            this.onBottomToTopSwipe();
                            return true;
                        }
                    } else {
                        Log.i(logTag, "Swipe was only " + Math.abs(deltaX) + " long vertically, need at least " + MIN_DISTANCE);
                        // return false; // We don't consume the event
                    }

                    return false; // no swipe horizontally and no swipe vertically
                }// case MotionEvent.ACTION_UP:
            }
            return false;
        }
    }

}
