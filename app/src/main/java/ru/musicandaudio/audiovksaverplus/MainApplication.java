package ru.musicandaudio.audiovksaverplus;

import android.app.Application;

import com.yandex.metrica.YandexMetrica;
import com.yandex.metrica.YandexMetricaConfig;

/**
 * Created by PK on 21.05.2018.
 */

// TODO
//    1. Переделать экран авторизации и приветсвия
//    3. Придумать с обложкой аудио в самом пелееере

public class MainApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        YandexMetricaConfig.Builder configBuilder = YandexMetricaConfig.newConfigBuilder("76827b1c-ab77-4a2c-9902-5f40b82969a4");
        YandexMetrica.activate(getApplicationContext(), configBuilder.build());
        YandexMetrica.enableActivityAutoTracking(this);
    }
}

