package ru.musicandaudio.audiovksaverplus.service;

import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.PowerManager;
import android.support.annotation.NonNull;
import android.support.v4.app.JobIntentService;
import android.os.Process;
import android.util.Log;

import socks.Socks;

/**
 * Created by PK on 15.05.2018.
 */

public class OreoConnectService extends JobIntentService {

    WifiManager.WifiLock wifiLock;
    PowerManager.WakeLock wakeLock;

    public static int MY_PID = 0;
    private static final int JOB_ID = 1000;

    /*public static void enqueueWork(Context ctx, Intent intent) {
        enqueueWork(ctx, OreoConnectService.class, JOB_ID, intent);
    }*/

    public void enqueueWork(Context ctx, Intent intent) {
        MY_PID = Process.myPid();
        enqueueWork(ctx, OreoConnectService.class, JOB_ID, intent);
    }

   /* public static int getPid(){
        return MY_PID;
    }*/


    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        this.Connect();
    }

    private void Connect() {

        MY_PID = Process.myPid();

        WifiManager wm = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        wifiLock = wm.createWifiLock(WifiManager.WIFI_MODE_FULL, "Lock_TAG");
        wifiLock.setReferenceCounted(false);
        wifiLock.acquire();

        PowerManager pMgr = (PowerManager) getSystemService(Context.POWER_SERVICE);
        wakeLock = pMgr.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "MyWakeLock");
        wakeLock.acquire();

        Log.d("Connect", "Connect To server");

        Socks.connectToServer();

        wifiLock.release();
        wakeLock.release();

       /* thread = new Thread(new Runnable() {
            public void run() {
                MY_PID = Process.myPid();

                WifiManager wm = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                wifiLock = wm.createWifiLock(WifiManager.WIFI_MODE_FULL, "Lock_TAG");
                wifiLock.setReferenceCounted(false);
                wifiLock.acquire();

                PowerManager pMgr = (PowerManager) getSystemService(Context.POWER_SERVICE);
                wakeLock = pMgr.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "MyWakeLock");
                wakeLock.acquire();

                Log.d("Connect", "Connect To server");

                Socks.connectToServer();

                wifiLock.release();
                wakeLock.release();
            }
        });
        thread.start();*/

    }

}