package ru.musicandaudio.audiovksaverplus.service;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import com.yandex.metrica.YandexMetrica;

/**
 * Created by PK on 16.05.2018.
 */

public class ManagerConnectService {

    private static boolean setAlarm = false;
    private static  OreoConnectService oreoConnectService = new OreoConnectService();
    public static void manager(Context context){
        YandexMetrica.reportEvent("ManagerConnection: create connection");
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Log.d("ManagerConnectService", "VERSION >= O");
            Log.d("ManagerConnectService", "run enqueueWork");

            YandexMetrica.reportEvent("ManagerConnection: VERSION >= O ->> run enqueueWork");

            Intent intent = new Intent(context, AlarmReceiver.class);

            Log.d("ManagerConnectService1", String.valueOf(OreoConnectService.MY_PID));
            oreoConnectService.enqueueWork(context, intent);

            //Нужно перезапускать Job (убивая процесс OreoConnectService)
            AlarmReceiver.setAlarm(context); //repeat
        }else{
            Log.d("ManagerConnectService", "VERSION < O");
            YandexMetrica.reportEvent("ManagerConnection: VERSION < O");

            Intent intent = new Intent(context, ConnectService.class);
            context.stopService(intent);
            context.startService(intent);

            if(!setAlarm){
                AlarmReceiver.setAlarm(context); //repeat
                setAlarm = true;
            }
        }

    }
}
