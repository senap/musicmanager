package ru.musicandaudio.audiovksaverplus.service;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.PowerManager;
import android.util.Log;


public class AlarmReceiver extends BroadcastReceiver {

    //private OreoConnectService oreoJobIntentService = OreoConnectService.getInstance();

    @Override
    public void onReceive(Context context, Intent intent) {

        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "");
        wl.acquire();

        ManagerConnectService.manager(context);

        //ManagerConnectService.setAlarm(context);
        Log.d("Alarm", "onReceive");
        wl.release();
    }

    public static void setAlarm(Context context)
    {

        AlarmManager am =( AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent i = new Intent(context, AlarmReceiver.class);
        // i.setAction("runnuble");

        PendingIntent pi = PendingIntent.getBroadcast(context, 0, i, 0);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            am.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + 1000 * 60 * 10, pi); // Millisec * Second * Minute
        }else{
            am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 1000 * 60 * 10, pi); // Millisec * Second * Minute
        }

       /* AlarmManager am =( AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent i = new Intent(context, AlarmReceiver.class);
       // i.setAction("runnuble");

        PendingIntent pi = PendingIntent.getBroadcast(context, 0, i, 0);
        if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
            am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 1000 * 60 * 10, pi); // Millisec * Second * Minute
        } else if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            am.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + 1000 * 60 * 1, pi); // Millisec * Second * Minute
        }else{
            am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 1000 * 60 * 10, pi); // Millisec * Second * Minute
        }*/

    }

    public static void cancelAlarm(Context context)
    {
        Intent intent = new Intent(context, AlarmReceiver.class);
        PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(sender);

    }
}
