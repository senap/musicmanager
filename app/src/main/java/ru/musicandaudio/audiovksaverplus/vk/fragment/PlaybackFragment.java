package ru.musicandaudio.audiovksaverplus.vk.fragment;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import ru.musicandaudio.audiovksaverplus.R;
import ru.musicandaudio.audiovksaverplus.vk.Player;
import ru.musicandaudio.audiovksaverplus.vk.model.Audio;

/**
 * Created by PK on 03.04.2018.
 */

public class PlaybackFragment extends Fragment {

    private static final Player playerInstance = Player.getInstance();
    private Player.Description description;
    private ProgressBar spinerTrackItem;
    private Context context;

    private TextView artist;
    private TextView title;
    private ImageView art;
    private ImageButton playPause;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_playback_controls, container, false);
        artist = view.findViewById(R.id.artist);
        title = view.findViewById(R.id.title);
        art = view.findViewById(R.id.album_art);
        playPause = view.findViewById(R.id.play_pause);

        spinerTrackItem = view.findViewById(R.id.spiner_track_item);

        description = playerInstance.getDescription();
        setupState(description);

        playerInstance.setListenerChange(new Player.OnStateListener() {
            @Override
            public void onChange(Audio audio, Player.Description description) {
                setupState(description);
            }
        });

        playPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(description.getState() == Player.Description.STATE_PLAYING){
                    playerInstance.pause();
                }else if(description.getState() == Player.Description.STATE_PAUSED){
                    playerInstance.play();
                }


            }
        });

        return view;
    }

    private void setupState(Player.Description description){
        if(description.getState() == Player.Description.STATE_LOADING) {
            art.setVisibility(View.INVISIBLE);
            spinerTrackItem.setVisibility(View.VISIBLE);
        }else {
            art.setVisibility(View.VISIBLE);
            spinerTrackItem.setVisibility(View.GONE);
        }


        artist.setText(description.getTitle());
        title.setText(description.getArtist());

        if(description.getState() != Player.Description.STATE_ERROR_DECODE) {
            if(description.getState() != Player.Description.STATE_PAUSED) {
                if (description.getImage() != null) {
                    //art.setImageBitmap(BitmapFactory.decodeByteArray(description.getImage(), 0, description.getImage().length));
                    art.setImageBitmap(description.getImage());
                }else{
                    art.setImageDrawable(
                            ContextCompat.getDrawable(context, R.drawable.audio_row_placeholder));
                }
                playPause.setImageDrawable(
                        ContextCompat.getDrawable(context,R.drawable.ic_pause_black_36dp));
            }else{
                playPause.setImageDrawable(
                        ContextCompat.getDrawable(context, R.drawable.ic_play_arrow_black_36dp));
            }

        }else{
            Toast.makeText(getContext(), "Errro decompile audio", Toast.LENGTH_SHORT).show();
        }
    }
}
