package ru.musicandaudio.audiovksaverplus.vk;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.view.Gravity;
import android.view.View;

import ru.musicandaudio.audiovksaverplus.PlayerActivity;
import ru.musicandaudio.audiovksaverplus.R;
import ru.musicandaudio.audiovksaverplus.vk.adapter.ViewPagerAdapter;
import ru.musicandaudio.audiovksaverplus.vk.fragment.MyAudioFragment;
import ru.musicandaudio.audiovksaverplus.vk.fragment.SearchAudioFragment;
import ru.musicandaudio.audiovksaverplus.vk.fragment.TopAudioFragment;
import ru.musicandaudio.audiovksaverplus.vk.model.Audio;
import ru.musicandaudio.audiovksaverplus.vk.util.PatternRegex;
import ru.musicandaudio.audiovksaverplus.vk.util.Util;

/**
 * Created by PK on 26.03.2018.
 */

// TODO 1 Вынести обработчки ошибок (Event from PLayer ) Сюда (В одно место) мз фрагметов
public class VkMainActivity extends AppCompatActivity {

    private static final Player playerInstance = Player.getInstance();
    private CardView cardView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vk_main);

        PatternRegex.loadJSCode(VkMainActivity.this);

        cardView = findViewById(R.id.controls_container);
        cardView.setVisibility(View.INVISIBLE);
        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(VkMainActivity.this, PlayerActivity.class);
                startActivity(i);
            }
        });

        Util.requestStoragePermission(VkMainActivity.this);
        Util.requestPhoneStatePermission(VkMainActivity.this);

        setupViewPager();

        registerReceiver(receiver, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));

        playerInstance.setListenerChange(new Player.OnStateListener(){
            @Override
            public void onChange(Audio audio, Player.Description description){
                if(description.getState() == Player.Description.STATE_PLAYING)
                    cardView.setVisibility(View.VISIBLE);
            }
        });

        TelephonyManager mgr = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
        if(mgr != null) {
            mgr.listen(phoneStateListener, PhoneStateListener.LISTEN_CALL_STATE);
        }

    }

    @Override
    protected void onResume() {
        try {
            Player.Description description = playerInstance.getDescription();
            registerReceiver(receiver, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
            if(description.getState() == Player.Description.STATE_PLAYING)
                cardView.setVisibility(View.VISIBLE);
        }catch (IllegalArgumentException e){

        }
        super.onResume();
    }

    @Override
    protected void onStop() {
        try {
            unregisterReceiver(receiver);
        }catch (IllegalArgumentException e){

        }
        super.onStop();
    }
    @Override
    protected void onDestroy() {
        try {
            unregisterReceiver(receiver);
        }catch (IllegalArgumentException e){

        }
        super.onDestroy();
    }


    private void setupViewPager() {
        ViewPager viewPager = findViewById(R.id.viewpager);
        //TabLayout tabLayout = findViewById(R.id.tabs);


        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new MyAudioFragment(), "Моя");
        adapter.addFragment(new SearchAudioFragment(), "Поиск");
        adapter.addFragment(new TopAudioFragment(), "Топ");
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(3);

        TabLayout tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            //Сообщение о том, что загрузка закончена
            if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)){
                View activityVkMain = findViewById(R.id.activity_vk_main);
                Snackbar mSnackbar;
                mSnackbar = Snackbar.make(activityVkMain, "Загрузка завершена, открыть папку ?", Snackbar.LENGTH_LONG);
                View view = mSnackbar.getView();
                CoordinatorLayout.LayoutParams params =(CoordinatorLayout.LayoutParams)view.getLayoutParams();
                params.gravity = Gravity.TOP;
                view.setLayoutParams(params);
                mSnackbar.setAction("Да", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startActivity(new Intent(DownloadManager.ACTION_VIEW_DOWNLOADS));
                    }
                }).setActionTextColor(getResources().getColor(R.color.bt_accent));
                mSnackbar.show();
            }

        }
    };


    PhoneStateListener phoneStateListener = new PhoneStateListener() {
        @Override
        public void onCallStateChanged(int state, String incomingNumber) {
            if (state == TelephonyManager.CALL_STATE_RINGING) {
                if(playerInstance.getDescription().getState() == Player.Description.STATE_PLAYING)
                    playerInstance.pause();
            } else if(state == TelephonyManager.CALL_STATE_IDLE) {
                if(playerInstance.getDescription().getState() == Player.Description.STATE_PAUSED)
                    playerInstance.play();
            } else if(state == TelephonyManager.CALL_STATE_OFFHOOK) {
                //A call is dialing, active or on hold
            }
            super.onCallStateChanged(state, incomingNumber);
        }
    };

}
