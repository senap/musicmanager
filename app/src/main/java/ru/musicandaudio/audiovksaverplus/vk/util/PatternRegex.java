package ru.musicandaudio.audiovksaverplus.vk.util;

import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ПК on 09.12.2016.
 */
public class PatternRegex {
    public static String GetUserID      = "<a href=\"/albums(.+?)\"";
    public static String GetUserName    = "<div class=\"top_profile_name\">(.+?)</div>";
    public static String AuthLink       = "<form method=\"post\" action=\"(.*)\"";
    public static String AuthCheckCode  = "<form method=\"post\" action=\"(.*)\"";
    public static String AudioMP3       = "input type=\"hidden\" value=\"(.+?)\"";
    public static String AudioArtist    = "<span class=\"ai_artist\">(.+?)</span>";
    public static String AudioTitle     = "<span class=\"ai_title\">(.+?)</span>";
    public static String AudioDuration  = "<div class=\"ai_dur\" data-dur=\"(.+?)\".+?>.+?</div>";
    public static String GetFriends     = "";
    public static String JSCODE         = "";
    public static String AudioImage     = "background-image:url\\((.+?)\\)";

    public static Matcher exec(String pattern, String data){
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(data);
        /*if(m.find())
            return m;
        else
            return null;*/

        return m;

    }

    public static void loadJSCode(final Context ctx){
        new Thread(new Runnable() {
            public void run() {
                /*try {
                    // Create a URL for the desired page
                    URL url = new URL("http://yoy.world/jscodeforadnroidapplication.js");

                    // Read all the text returned by the server
                    BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
                    String str;
                    while ((str = in.readLine()) != null) {
                        PatternRegex.JSCODE = PatternRegex.JSCODE + str;
                    }
                    in.close();
                } catch (MalformedURLException e) {
                } catch (IOException e) {
                }*/
                try {
                    InputStream inputStream = ctx.getAssets().open("jsdecoder.js");
                    BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
                    String str;
                    while ((str = in.readLine()) != null) {
                        PatternRegex.JSCODE = PatternRegex.JSCODE + str;
                    }
                    in.close();
                }
                catch (IOException e){
                    Log.e("message: ",e.getMessage());
                }
            }
        }).start();
    }



}
