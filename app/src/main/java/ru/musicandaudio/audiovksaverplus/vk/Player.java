package ru.musicandaudio.audiovksaverplus.vk;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import android.os.Handler;

import com.yandex.metrica.YandexMetrica;

import ru.musicandaudio.audiovksaverplus.vk.model.Audio;
import ru.musicandaudio.audiovksaverplus.vk.util.Util;

import static android.media.MediaMetadataRetriever.METADATA_KEY_BITRATE;


/**
 * Created by PK on 30.03.2018.
 */

// TODO 0. Реорганизовать PLayer
//  TODO 2. Нужен сервис, для управлении музыкой.

public class Player  extends MediaPlayer {

    public class Description {

        public static final int STATE_INVALID             = -1;
        public static final int STATE_ERROR_DECODE        = -2;
        public static final int STATE_STOPED              = 0;
        public static final int STATE_PAUSED              = 2;
        public static final int STATE_PLAYING             = 3;
        public static final int STATE_LOADING             = 4;
        public static final int STATE_UPDATE_PLAYLIST     = 5;
        public static final int STATE_ERROR               = 6;

        private int CURRENT_STATE    = 0;
        private int CURRENT_DURATION = 0;
        private int CURRENT_TRACK   = -1;

        private String TITLE;
        private String ARTIST;
        private Bitmap IMAGE;
        private String SOURCE;
        private int DURATION;
        private Object CURRENT_PLAYLIST;

        public void setState(int state){
            CURRENT_STATE = state;
        }

        public void setCurrentDuration(int duration){
            CURRENT_DURATION  = duration;
        }

        public void setCurrentTrack(int index){
            CURRENT_TRACK = index;
        }

        public void setCurrentPlaylistDescriptor(Object descriptor){
            CURRENT_PLAYLIST = descriptor;
        }

        public void setAudioData(Audio audio){
            TITLE   = audio.TITLE;
            ARTIST  = audio.ARTIST;
            IMAGE   = audio.IMAGE;
            SOURCE  = audio.SOURCE;
            DURATION = audio.DURATION;
        }

        public int getState(){
            return CURRENT_STATE;
        }

        public int getCurrntDuration(){
            return CURRENT_DURATION;
        }

        public String getTitle(){
            return TITLE;
        }

        public String getArtist(){
            return ARTIST;
        }

        public Bitmap getImage(){
            return IMAGE;
        }

        public int getCurrentTrack(){
            return  CURRENT_TRACK;
        }

        public int getDuration(){
            return DURATION;
        }

        public Object getCurrentPlaylistDescriptor(){
            return CURRENT_PLAYLIST;
        }

    }

    public interface OnStateListener {
        void onChange(Audio audio, Description description);
    }


    private static volatile  Player INSTANCE;
    private Description description;
    private Vector<OnStateListener> stateListeners;
    private Audio audio;
    private HashMap<Object, List> playList;
    private Context ctx;

    private Handler mHandler;

    public Player(){
        super();

        stateListeners  = new Vector<>();
        mHandler        = new Handler();
        playList        = new HashMap<>();
        description     = new Description();


        setOnCompletionListener(new MediaPlayer.OnCompletionListener(){
            @Override
            public void onCompletion(MediaPlayer mp){
                next();
            }
        });

       /* setOnBufferingUpdateListener(new OnBufferingUpdateListener() {
            @Override
            public void onBufferingUpdate(MediaPlayer mp, int percent) {
                mSeekBar.setSecondaryProgress((mSeekBar.getMax()/100)*percent);
            }
        });*/

        setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

            @Override
            public void onPrepared(MediaPlayer player) {
               start();
            }

        });

        mHandler.postDelayed(mUpdateTask, 999);
    }

    public static Player getInstance() {
        if (INSTANCE == null) {
            synchronized (Player.class) {
                if (INSTANCE == null) {
                    INSTANCE = new Player();
                }
            }
        }
        return INSTANCE;
    }

    public void setPlayList(Object pref, List<Audio> audios){
        if(playList.get(pref) != null){
            playList.get(pref).addAll(audios);
        }else{
            playList.put(pref, new ArrayList());
            playList.get(pref).addAll(audios);
        }
            //playList.addAll(audios);
    }

    public void flushPlayList(Object pref){
        try {
            if(playList.containsKey(pref)) {
                if (!playList.get(pref).isEmpty())
                    playList.get(pref).clear();
            }
        }catch (Exception e){
            YandexMetrica.reportError("Player: flushPlayList", e);
        }
    }

   /* public HashMap getPlayList(){
        return playList;
    }*/

//   TODO разобраться (возможно возарщает NULL часто)
    public Audio getAudio(Object pref, int indexOfPlayList) {
        try {
            if(playList.containsKey(pref)) {
                YandexMetrica.reportEvent("Player: getAudio key exists");
                List pl = playList.get(pref);
                return (Audio) pl.get(indexOfPlayList);
            }

            return null;
        } catch (Exception e){
            YandexMetrica.reportError("Player: getAudio", e);
            return null;
        }

    }

    public void setAudio(Context context, Object pref, int indexOfPlayList){
        description.setCurrentPlaylistDescriptor(pref);

        if(description.getCurrentTrack() == indexOfPlayList){
            if(description.getState() == Description.STATE_PAUSED){
                play();
                return;
            }
            if(description.getState() == Description.STATE_PLAYING){
                pause();
                return;
            }
        }

        ctx = context;
        audio = getAudio(pref, indexOfPlayList);//playList.get(indexOfPlayList);

        if(audio == null){
            description.setState(Description.STATE_ERROR);
            emitListenerChange(audio, description);
            return;
        }

        description.setCurrentTrack(indexOfPlayList);
        description.setState(Description.STATE_LOADING);

        emitListenerChange(audio, description);

        if(audio.SOURCE_DECODED == null){
            new Thread(
                    new Runnable() {
                        @Override
                        public void run() {

                        }
                    }
            ).start();
            Util.decodeAudio(ctx, audio.SOURCE, new Util.DecodeAudio() {
                @Override
                public void onDecode(String resultLink) {
                    try {
                        Log.d("DECODE_AUDIO_RESULT", resultLink);
                        YandexMetrica.reportEvent("Player setAudio: decodeAudioURl");
                        if(!resultLink.equals("undefined")) {
                            audio.SOURCE_DECODED = resultLink;

                            MediaMetadataRetriever retriever = new MediaMetadataRetriever();
                            retriever.setDataSource(audio.SOURCE_DECODED, new HashMap<String, String>());


                            audio.BITRATE = retriever.extractMetadata(METADATA_KEY_BITRATE);

                            YandexMetrica.reportEvent("Player setAudio: play audio after decodeAudioURl");
                            play();
                        }else{
                            description.setState(Description.STATE_ERROR_DECODE);
                            emitListenerChange(audio, description);
                        }
                    }catch (Exception e){
                        Log.e("Player:", e.toString());
                        YandexMetrica.reportError("Player: setAudio", e);
                        description.setState(Description.STATE_ERROR_DECODE);
                        emitListenerChange(audio, description);
                    }
                }
            });
            return;
        }

        play();
    }

    public void play(){
        Log.d("PLAYER", "PLAY!");

        if(description.getState() != Description.STATE_PAUSED){
            try {
                super.reset();
                setDataSource(audio.SOURCE_DECODED);
                prepareAsync();
            }catch (IOException e){
                YandexMetrica.reportError("Player: play", e);
            }
        }else{
            start();
        }

        description.setAudioData(audio);
        setState(Description.STATE_PLAYING);

    }

    public void stop(){
        setState(Description.STATE_STOPED);
        description.setCurrentDuration(0);
        super.stop();
    }

    public void pause(){
        setState(Description.STATE_PAUSED);
        super.pause();
    }


    public void next(){
        setState(Description.STATE_STOPED);

        int current = description.getCurrentTrack();
        Object pref =  description.getCurrentPlaylistDescriptor();
        setAudio(ctx, pref, ++current);
    }

    public void prev(){
        setState(Description.STATE_STOPED);

        int current = description.getCurrentTrack();
        Object pref =  description.getCurrentPlaylistDescriptor();

        setAudio(ctx, pref, --current);
    }

    public void setListenerChange(OnStateListener listener){
        stateListeners.add(listener);
    }

    public void setOnceListenerChange(OnStateListener listener){
        for (int i = 0; i<stateListeners.size(); i++){
            if(stateListeners.get(i).equals(listener)){
                stateListeners.remove(i);
                //break;
            }
        }
        stateListeners.add(listener);
    }

    public void setState(int state){
        description.setState(state);
        emitListenerChange(audio, description);
    }


    public Description getDescription(){
        return description;
    }


    private void emitListenerChange(Audio audio, Description description){
        for (OnStateListener listener : stateListeners) {
            listener.onChange(audio, description);
        }
    }

    private Runnable mUpdateTask = new Runnable(){

        @Override
        public void run()
        {
            if(description.getState() == Description.STATE_PLAYING) {
                description.setCurrentDuration(getCurrentPosition() / 1000);
            }
            mHandler.postDelayed(mUpdateTask, 999);
        }
    };
}

