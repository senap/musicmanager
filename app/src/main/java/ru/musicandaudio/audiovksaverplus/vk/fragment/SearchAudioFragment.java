package ru.musicandaudio.audiovksaverplus.vk.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.badoo.mobile.util.WeakHandler;
import com.yandex.metrica.YandexMetrica;

import java.io.IOException;
import java.util.ArrayList;

import ru.musicandaudio.audiovksaverplus.R;
import ru.musicandaudio.audiovksaverplus.vk.Player;
import ru.musicandaudio.audiovksaverplus.vk.adapter.RecyclerAdapter;
import ru.musicandaudio.audiovksaverplus.vk.model.Audio;
import ru.musicandaudio.audiovksaverplus.vk.util.HttpWrapperRequest;
import ru.musicandaudio.audiovksaverplus.vk.util.Util;

/**
 * Created by PK on 28.03.2018.
 */

// TODO Добавить прелоадер (под тормаживает, решить проблему)

public class SearchAudioFragment extends Fragment {

    private Button buttonReload;
    private RecyclerView recycleView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView.LayoutManager mLayoutManager;
    private WeakHandler handler;
    private ConstraintLayout error;

    private EditText searchText;
    //private RelativeLayout fragmentSearchAudio;
    private View viewCurrent;

    private int OFFSET = 0;
    private String PLAYER_TAG = "SEARCH";
    private boolean LOADING = false;
    private String SEARCH_PARAM = "Звуки природы";

    private RecyclerAdapter mAdapter;
    private static final Player playerInstance = Player.getInstance();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        playerInstance.setListenerChange(new Player.OnStateListener() {
            @Override
            public void onChange(Audio audio, Player.Description description) {
                if(description.getState() == Player.Description.STATE_ERROR) {
                    showButtonUpdate();
                    swipeRefreshLayout.setRefreshing(false);
                }
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_vk_search_audio, container, false);


        handler = new WeakHandler();
        buttonReload = view.findViewById(R.id.button_reload);

        error = view.findViewById(R.id.error);
        searchText = view.findViewById(R.id.searchText);
        //fragmentSearchAudio = view.findViewById(R.id.relativeLayout4);


        initUiControls();
        setupSwipeRefreshLayout(view);
        setupRecyclerView(view);
        loadSearchMusic(OFFSET, SEARCH_PARAM);
        setupScrollPagination();

        viewCurrent = view;

        return view;
    }


    private void initUiControls(){
        searchText.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if(s.length() != 0) {
                    // вынести в отдельный метод (clean data view)
                    mAdapter = new RecyclerAdapter(new ArrayList<Audio>(), itemClickCallback);
                    recycleView.setAdapter(mAdapter);
                    OFFSET = 0;
                    playerInstance.flushPlayList(PLAYER_TAG);


                    SEARCH_PARAM = s.toString();
                    loadSearchMusic(OFFSET, SEARCH_PARAM);
                }
            }
        });

        buttonReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadSearchMusic(OFFSET, SEARCH_PARAM);
            }
        });
    }

    private void loadSearchMusic(int offset, String searchParam)  {
        /*if(searchParam != null){
            // вынести в отдельный метод (clean data view)
            mAdapter = new RecyclerAdapter(new ArrayList<Audio>(), itemClickCallback);
            recycleView.setAdapter(mAdapter);
            OFFSET = 0;
            playerInstance.flushPlayList(PLAYER_TAG);
        }else{
            searchParam = "Звуки природы";
        }*/
        if(!LOADING) {
            LOADING = true;

            swipeRefreshLayout.setRefreshing(true);
            try {
                HttpWrapperRequest httpWrapperRequest = new HttpWrapperRequest(getContext());
                httpWrapperRequest.SerachAudio(offset, searchParam, new HttpWrapperRequest.VKActionsAudio() {
                    @Override
                    public void onSuccess(final ArrayList data) {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                if (data.size() > 0) {
                                    AppMetrikaEventWrapper("loadSearchMusic data > 0");

                                    error.setVisibility(View.INVISIBLE);
                                    playerInstance.setPlayList(PLAYER_TAG, data);

                                    if (mAdapter == null) {
                                        mAdapter = new RecyclerAdapter(data, itemClickCallback);
                                        recycleView.setAdapter(mAdapter);
                                    } else {
                                        mAdapter.push(data);
                                        mAdapter.notifyDataSetChanged();
                                    }
                                } else {
                                    AppMetrikaEventWrapper("loadSearchMusic data < 0");
                                    showButtonUpdate();
                                }

                                LOADING = false;
                                swipeRefreshLayout.setRefreshing(false);
                            }
                        });
                    }

                    @Override
                    public void onFail(final String data) {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                AppMetrikaEventWrapper("loadSearchMusic onFail" + data);
                                showButtonUpdate();
                                LOADING = false;
                                swipeRefreshLayout.setRefreshing(false);
                                Log.e(PLAYER_TAG, data);
                            }
                        });
                    }

                    @Override
                    public void onError(final Exception e) {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                AppMetrikaEventWrapper("loadSearchMusic onError");
                                YandexMetrica.reportError("loadSearchMusic onError:", e);

                                showButtonUpdate();
                                swipeRefreshLayout.setRefreshing(false);
                                Log.e(PLAYER_TAG, e.toString());
                            }
                        });
                    }
                });
            } catch (IOException e) {
                AppMetrikaEventWrapper("loadSearchMusic Exception");
                YandexMetrica.reportError("loadSearchMusic:", e);


                Log.e(PLAYER_TAG, e.toString());
                showButtonUpdate();
                LOADING = false;
                swipeRefreshLayout.setRefreshing(false);
            }
        }
    }

    private void setupSwipeRefreshLayout(View v) {
        swipeRefreshLayout = v.findViewById(R.id.swipe_refresh);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimaryDark);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mAdapter = new RecyclerAdapter(new ArrayList<Audio>(), itemClickCallback);
                recycleView.setAdapter(mAdapter);
                OFFSET = 0;
                playerInstance.flushPlayList(PLAYER_TAG);
                loadSearchMusic(OFFSET, SEARCH_PARAM);
            }
        });
    }

    private void setupRecyclerView(View v){
        recycleView =  v.findViewById(R.id.recycler_view);
        mLayoutManager = new LinearLayoutManager(getContext());
        recycleView.setLayoutManager(mLayoutManager);
    }

    private void showButtonUpdate(){
        Audio audio = playerInstance.getAudio(PLAYER_TAG, 0);
        if(audio == null) { //if empty playlist
            recycleView.setVisibility(View.VISIBLE);
            error.setVisibility(View.VISIBLE);
        }
    }

    private void setupScrollPagination(){
        recycleView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                LinearLayoutManager layoutManager=LinearLayoutManager.class.cast(recyclerView.getLayoutManager());
                int totalItemCount = layoutManager.getItemCount();
                int lastVisible = layoutManager.findLastVisibleItemPosition();

                boolean endHasBeenReached = lastVisible + 10 >= totalItemCount;
                if (totalItemCount > 0 && endHasBeenReached && !LOADING) {
                    OFFSET += 30;
                    loadSearchMusic(OFFSET, SEARCH_PARAM);
                }
            }
        });
    }


    private final RecyclerAdapter.ItemClickCallback itemClickCallback =
            new RecyclerAdapter.ItemClickCallback() {
                @Override
                public void onItemClick(@NonNull int indexOfPlayList) {
                    InputMethodManager imm = (InputMethodManager)getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(viewCurrent.getWindowToken(), 0);

                    playerInstance.setAudio(getContext(), PLAYER_TAG, indexOfPlayList);
                    //Log.d("MY_MUSIC_FRAGMENT", audio.ARTIST);
                }

                @Override
                public void onItemClickSave(@NonNull int indexOfPlayLis){
                    Audio audio =  playerInstance.getAudio(PLAYER_TAG, indexOfPlayLis);
                    if(audio != null)
                        Util.saveAudio(getContext(), audio);
                    else
                        showButtonUpdate();
                }
            };

    private void AppMetrikaEventWrapper(String log){
        YandexMetrica.reportEvent(PLAYER_TAG + "_SearchAudioFragment:" + log);
    }
}
