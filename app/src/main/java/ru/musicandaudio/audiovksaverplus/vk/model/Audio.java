package ru.musicandaudio.audiovksaverplus.vk.model;

import android.graphics.Bitmap;

import java.io.Serializable;

/**
 * Created by PK on 29.03.2018.
 */

public class Audio implements Serializable {

    public String TITLE;
    public String ALBUM;
    public String ARTIST;
    public String GENRE;
    public String SOURCE;
    public String SOURCE_DECODED;
    public String SOURCE_IMAGE;
    public Bitmap IMAGE;
    public int DURATION;
    public String BITRATE;

}