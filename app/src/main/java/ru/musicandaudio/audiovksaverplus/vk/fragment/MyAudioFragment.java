package ru.musicandaudio.audiovksaverplus.vk.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.badoo.mobile.util.WeakHandler;
import com.yandex.metrica.YandexMetrica;

import java.io.IOException;
import java.util.ArrayList;

import ru.musicandaudio.audiovksaverplus.R;
import ru.musicandaudio.audiovksaverplus.vk.Player;
import ru.musicandaudio.audiovksaverplus.vk.adapter.RecyclerAdapter;
import ru.musicandaudio.audiovksaverplus.vk.model.Audio;
import ru.musicandaudio.audiovksaverplus.vk.model.User;
import ru.musicandaudio.audiovksaverplus.vk.util.HttpWrapperRequest;
import ru.musicandaudio.audiovksaverplus.vk.util.Util;

/**
 * Created by PK on 28.03.2018.
 */

//    TODO Сделать Parent fragment и реализовать все там

public class MyAudioFragment extends Fragment {

    private Button buttonReload;
    private ConstraintLayout error;
    private RecyclerView recycleView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView.LayoutManager mLayoutManager;
    private WeakHandler handler;

    private int OFFSET = 0;
    private String PLAYER_TAG = "MY";
    private boolean LOADING = false;

    private RecyclerAdapter mAdapter;
    private static final Player playerInstance = Player.getInstance();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        playerInstance.setListenerChange(new Player.OnStateListener() {
            @Override
            public void onChange(Audio audio, Player.Description description) {
                if(description.getState() == Player.Description.STATE_ERROR) {
                    AppMetrikaEventWrapper("Player return STATE_ERROR");
                    showButtonUpdate();
                    swipeRefreshLayout.setRefreshing(false);
                }
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_vk_my_audio, container, false);
        buttonReload = view.findViewById(R.id.button_reload);
        error = view.findViewById(R.id.error);

        buttonReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadMyMusic(OFFSET);
            }
        });


        handler = new WeakHandler();

        setupSwipeRefreshLayout(view);
        setupRecyclerView(view);
        loadMyMusic(OFFSET);
        setupScrollPagination();

        return view;
    }

    private void loadMyMusic(int offset)  {
        if(!LOADING) {
            LOADING = true;
            swipeRefreshLayout.setRefreshing(true);
            try {
                HttpWrapperRequest httpWrapperRequest = new HttpWrapperRequest(getContext());
                httpWrapperRequest.MyAudio(offset, User.ID, new HttpWrapperRequest.VKActionsAudio() {
                    @Override
                    public void onSuccess(final ArrayList data) {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                if (data.size() > 0) {
                                    AppMetrikaEventWrapper("loadMyMusic data > 0");

                                    error.setVisibility(View.INVISIBLE);
                                    playerInstance.setPlayList(PLAYER_TAG, data);

                                    if (mAdapter == null) {
                                        mAdapter = new RecyclerAdapter(data, itemClickCallback);
                                        recycleView.setAdapter(mAdapter);
                                    } else {
                                        mAdapter.push(data);
                                        mAdapter.notifyDataSetChanged();
                                    }
                                } else {
                                    AppMetrikaEventWrapper("loadMyMusic < 0");
                                    showButtonUpdate();
                                }
                                LOADING = false;
                                swipeRefreshLayout.setRefreshing(false);
                            }
                        });
                    }

                    @Override
                    public void onFail(final String data) {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                AppMetrikaEventWrapper("loadMyMusic onFail" + data);
                                showButtonUpdate();
                                LOADING = false;
                                swipeRefreshLayout.setRefreshing(false);
                                Log.e(PLAYER_TAG, data);
                            }
                        });
                    }

                    @Override
                    public void onError(final Exception e) {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                showButtonUpdate();
                                AppMetrikaEventWrapper("loadMyMusic onError");
                                YandexMetrica.reportError("loadMyMusic onError", e);
                                LOADING = false;
                                swipeRefreshLayout.setRefreshing(false);
                                Log.e(PLAYER_TAG, e.toString());
                            }
                        });
                    }
                });
            } catch (IOException e) {
                LOADING = false;
                swipeRefreshLayout.setRefreshing(false);
                showButtonUpdate();
                Log.e(PLAYER_TAG, e.toString());
                AppMetrikaEventWrapper("loadMyMusic Exception");
                YandexMetrica.reportError("MyAudioFragment loadmusic:", e);
            }
        }
    }

    private void setupSwipeRefreshLayout(View v) {
        swipeRefreshLayout = v.findViewById(R.id.swipe_refresh);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimaryDark);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mAdapter = new RecyclerAdapter(new ArrayList<Audio>(), itemClickCallback);
                recycleView.setAdapter(mAdapter);
                OFFSET = 0;
                playerInstance.flushPlayList(PLAYER_TAG);
                loadMyMusic(OFFSET);
            }
        });
    }

    private void setupRecyclerView(View v){
        recycleView =  v.findViewById(R.id.recycler_view);
        mLayoutManager = new LinearLayoutManager(getContext());
        recycleView.setLayoutManager(mLayoutManager);
    }

    private void setupScrollPagination(){
        recycleView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                LinearLayoutManager layoutManager=LinearLayoutManager.class.cast(recyclerView.getLayoutManager());
                int totalItemCount = layoutManager.getItemCount();
                int lastVisible = layoutManager.findLastVisibleItemPosition();

                boolean endHasBeenReached = lastVisible + 5 >= totalItemCount;
                if (totalItemCount > 0 && endHasBeenReached && !LOADING) {
                    OFFSET += 30;
                    loadMyMusic(OFFSET);
                }
            }
        });
    }

    private void showButtonUpdate(){
        Audio audio = playerInstance.getAudio(PLAYER_TAG, 0);
        if(audio == null) { //if empty playlist
            recycleView.setVisibility(View.VISIBLE);
            error.setVisibility(View.VISIBLE);
        }
    }

    private final RecyclerAdapter.ItemClickCallback itemClickCallback =
            new RecyclerAdapter.ItemClickCallback() {
                @Override
                public void onItemClick(@NonNull int indexOfPlayList) {
                    AppMetrikaEventWrapper("PLAY TRACK");
                    playerInstance.setAudio(getContext(), PLAYER_TAG, indexOfPlayList);
                    //Log.d("MY_MUSIC_FRAGMENT", audio.ARTIST);
                }

                @Override
                public void onItemClickSave(@NonNull int indexOfPlayLis){
                    AppMetrikaEventWrapper("SAVE TRACK");
                    Audio audio =  playerInstance.getAudio(PLAYER_TAG, indexOfPlayLis);
                    if(audio != null) {
                        Util.saveAudio(getContext(), playerInstance.getAudio(PLAYER_TAG, indexOfPlayLis));
                    }else {
                        showButtonUpdate();
                    }
                }
            };


    private void AppMetrikaEventWrapper(String log){
        YandexMetrica.reportEvent(PLAYER_TAG + "_MyAudioFragment:" + log);
    }
}
