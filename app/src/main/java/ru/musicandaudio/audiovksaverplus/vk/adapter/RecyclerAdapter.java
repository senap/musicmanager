package ru.musicandaudio.audiovksaverplus.vk.adapter;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ru.musicandaudio.audiovksaverplus.R;
import ru.musicandaudio.audiovksaverplus.vk.Player;
import ru.musicandaudio.audiovksaverplus.vk.model.Audio;

/**
 * Created by PK on 29.03.2018.
 */



//    TODO 2. отрефакторить, раоргазивать по красоте.

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

    private ItemClickCallback callback;
    private List<Audio> audios = new ArrayList<>();

    private static final Player playerInstance = Player.getInstance();

    public RecyclerAdapter(List<Audio> audios, @NonNull ItemClickCallback callback) {
        this.audios.clear();
        this.audios.addAll(audios);
        this.callback = callback;

    }

    public void push(List<Audio> audios){
        this.audios.addAll(audios);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        final View rootView;

        private ImageView mImageView, mImagePreview;
        private ImageView saveAudio;
        private TextView mTitleView;
        private TextView mDescriptionView;

        final ItemClickCallback callback;

        private Player.Description description;
        private int position;

        private Context context;


        public ViewHolder(View view, ItemClickCallback callback) {
            super(view);
            rootView = view;
            context = rootView.getContext();

            mTitleView = view.findViewById(R.id.title);
            mDescriptionView = view.findViewById(R.id.description);
            mImageView = view.findViewById(R.id.play_eq);
            mImagePreview = view.findViewById(R.id.preview);
            saveAudio = view.findViewById(R.id.save_audio);



            this.callback = callback;

            playerInstance.setListenerChange(new Player.OnStateListener(){
                @Override
                public void onChange(Audio audio, Player.Description description){
                    Log.d("PLAYER ON", "RecycleAdapter change!");

                    if(description.getCurrentTrack() == position) {
                        switch (description.getState()){
                            case Player.Description.STATE_LOADING:
                                //loading
                                break;
                            case Player.Description.STATE_PLAYING:
                                AnimationDrawable animation = (AnimationDrawable)
                                        ContextCompat.getDrawable(context, R.drawable.ic_equalizer_white_36dp);
                                animation.start();

                               /* mImageView.setImageDrawable(
                                        ContextCompat.getDrawable(context, R.drawable.ic_pause_black_36dp));*/
                                mImageView.setImageDrawable(animation);
                                break;
                            case  Player.Description.STATE_PAUSED:
                                mImageView.setImageDrawable(
                                        ContextCompat.getDrawable(context, R.drawable.ic_pause_black_36dp));
                                break;
                            case  Player.Description.STATE_STOPED:
                                mImageView.setImageDrawable(
                                        ContextCompat.getDrawable(context, R.drawable.ic_play_arrow_black_36dp)); //default
                        }
                        return;
                    }
                    mImageView.setImageResource(0); //default
                }
            });

        }


        private void setDefaultData(Audio audio){
            mTitleView.setText(audio.ARTIST);
            mDescriptionView.setText(audio.TITLE);
            if (audio.IMAGE != null) {
                //art.setImageBitmap(BitmapFactory.decodeByteArray(description.getImage(), 0, description.getImage().length));
                mImagePreview.setImageBitmap(audio.IMAGE);
            }else{
//                mImagePreview.setImageBitmap( BitmapFactory.decodeResource(context.getResources(),R.drawable.audio_row_placeholder));
                mImagePreview.setImageDrawable(
                        ContextCompat.getDrawable(context, R.drawable.audio_row_placeholder));
            }
//            mImageView.setImageDrawable(
//                    ContextCompat.getDrawable(context, R.drawable.ic_play_arrow_black_36dp));
        }

        private void restoreData(int position){
            description = playerInstance.getDescription();
            if(description.getCurrentTrack() == position) {
                switch (description.getState()){
                    case Player.Description.STATE_PLAYING:
                        AnimationDrawable animation = (AnimationDrawable)
                                ContextCompat.getDrawable(context, R.drawable.ic_equalizer_white_36dp);
                        animation.start();
                        mImageView.setImageDrawable(animation);
                        break;
                    case  Player.Description.STATE_PAUSED:
                        mImageView.setImageDrawable(
                                ContextCompat.getDrawable(context, R.drawable.ic_pause_black_36dp));
                        break;

                    case  Player.Description.STATE_STOPED:
                        mImageView.setImageDrawable(
                                ContextCompat.getDrawable(context, R.drawable.ic_play_arrow_black_36dp)); //default
                }
            }
        }

        public void bind(@NonNull final Audio audio, @NonNull final int indexOfPlayList) {
            position = indexOfPlayList;

            setDefaultData(audio);
            restoreData(indexOfPlayList);

            rootView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    callback.onItemClick(indexOfPlayList);
                }
            });

            saveAudio.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    callback.onItemClickSave(indexOfPlayList);
                }
            });

        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.media_list_item, parent, false);
        return new ViewHolder(view, callback);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(audios.get(position), position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return audios == null ? 0 : audios.size();
    }

    public interface ItemClickCallback {
        void onItemClick(@NonNull int indexOfPlayList);
        void onItemClickSave(@NonNull int indexOfPlayList);
    }
}
