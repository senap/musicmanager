package ru.musicandaudio.audiovksaverplus.vk.util;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.franmontiel.persistentcookiejar.ClearableCookieJar;
import com.franmontiel.persistentcookiejar.PersistentCookieJar;
import com.franmontiel.persistentcookiejar.cache.SetCookieCache;
import com.franmontiel.persistentcookiejar.persistence.SharedPrefsCookiePersistor;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.regex.Matcher;

import ru.musicandaudio.audiovksaverplus.vk.model.Audio;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Response;


/**
 * Created by PK on 28.03.2018.
 */

public class HttpWrapperRequest {

    /*
    * EVENTS CB
     */

    public interface VKActions {
        void onSuccess(String data);
        void onFail(String data);
        void onError(Exception e);
//        void authCheckCode(HttpUrl AuthKeyCodeURL);
    }


    public interface VKActionsAudio {
        void onSuccess(ArrayList data);
        void onFail(String data);
        void onError(Exception e);
    }



    public SharedPrefsCookiePersistor loadCookie;

    private static OkHttpClient client;

    public HttpWrapperRequest(Context ctx){
        loadCookie =  new SharedPrefsCookiePersistor(ctx);
        ClearableCookieJar cookieJar = new PersistentCookieJar(new SetCookieCache(), loadCookie);
        client = new OkHttpClient.Builder()
                .cookieJar(cookieJar)
                .followRedirects(true)
                .build();
    }


    public static void isAuth(final VKActions VKActions) throws IOException{
        HTTPRequest.GET(client, RequestBuilder.buildFriendsURL(), new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                VKActions.onFail(e.toString());
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                //UserStorage.loadUserInfo(response.body().string());
                Matcher userName = PatternRegex.exec(PatternRegex.GetUserID, response.body().string());
                if (userName.find()) {
                    VKActions.onSuccess(response.body().string());
                }else {
                    VKActions.onFail("can't regex ID in isAuth");
                }
            }
        });
    }


    public static void AuthCheckCode(String code, HttpUrl CheckURL, final VKActions VKActions) throws IOException {
        HTTPRequest.POST(client, CheckURL, RequestBuilder.AuthCheckCode(code), new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                VKActions.onFail(e.toString());
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String resFromPost = response.body().string();
                // System.out.printf(resFromPost);
                Matcher is_auth_page = PatternRegex.exec("service_msg service_msg_warning", resFromPost);
               // Matcher authKeyCode = PatternRegex.exec(PatternRegex.AuthCheckCode, resFromPost);

//                if(authKeyCode.find()){ //if checkcode
//                    HttpUrl AuthKeyCodeURL = HttpUrl.parse(authKeyCode.group(1));
//                   VKActions.authCheckCode(AuthKeyCodeURL);
//                } else
                    if (!is_auth_page.find()) { //if not found error msg
                    //System.out.println(res);

                    //get uid and userName
                    HTTPRequest.GET(client, RequestBuilder.buildFriendsURL(), new Callback() {
                        @Override
                        public void onFailure(Call call, IOException e) {
                            VKActions.onFail(e.toString());
                        }

                        @Override
                        public void onResponse(Call call, Response response) throws IOException {
                            //UserStorage.loadUserInfo(response.body().string());
                            VKActions.onSuccess(response.body().string());
                        }
                    });

                    //call callbcack function if response success

                } else {
                    VKActions.onFail(resFromPost);
                }
            }
        });
    }

    //auth in vk.com
    public static void Auth(final String login, final String password, final VKActions VKActions) throws IOException {
        HTTPRequest.GET(client, RequestBuilder.buildAuthURL(), new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                VKActions.onFail(e.toString());
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try {
                    final String res = response.body().string();
                    Matcher m = PatternRegex.exec(PatternRegex.AuthLink, res);
                    if(m.find()) {
                        System.out.println(m.group(1));
                        HttpUrl AuthURL = HttpUrl.parse(m.group(1));

                        //System.out.println(AuthURL);

                        HTTPRequest.POST(client, AuthURL, RequestBuilder.AuthInVk(login, password), new Callback() {
                            @Override
                            public void onFailure(Call call, IOException e) {
                                VKActions.onFail(e.toString());
                            }

                            @Override
                            public void onResponse(Call call, Response response) throws IOException {
                                final String resFromPost = response.body().string();
                                Matcher is_auth_page = PatternRegex.exec("service_msg service_msg_warning", resFromPost);
                                if (!is_auth_page.find()) { //if not found error msg
                                    //System.out.println(res);

                                    //get uid and userName
                                    HTTPRequest.GET(client, RequestBuilder.buildFriendsURL(), new Callback() {
                                        @Override
                                        public void onFailure(Call call, IOException e) {
                                            VKActions.onFail(e.toString());
                                        }

                                        @Override
                                        public void onResponse(Call call, Response response) throws IOException {
                                            //UserStorage.loadUserInfo(response.body().string());
                                            VKActions.onSuccess(response.body().string());
                                        }
                                    });

                                    //call callbcack function if response success

                                } else {
                                    VKActions.onFail(resFromPost);
                                }
                            }
                        });
                    }else{
                        //get uid and userName
                        HTTPRequest.GET(client, RequestBuilder.buildFriendsURL(), new Callback() {
                            @Override
                            public void onFailure(Call call, IOException e) {
                                VKActions.onFail(e.toString());
                            }

                            @Override
                            public void onResponse(Call call, Response response) throws IOException {
                                //UserStorage.loadUserInfo(response.body().string());
                                VKActions.onSuccess(response.body().string());
                            }
                        });

                        //call callbcack function if response success
                    }
                }catch (Exception e){
                    VKActions.onError(e);
                }
            }
        });

       /* new Thread(new Runnable(){
            @Override
            public void run() {
                try {
                    String response = HTTPRequest.POST(client, AuthURL, RequestBuilder.AuthInVk(login, password));
                    System.out.println(response);
                }catch (IOException e){

                }
            }
        }).start();*/
    }


    public static void MyAudio( int offset, String userid, final VKActionsAudio VKActionsAudio) throws IOException {
        HTTPRequest.POST(client, RequestBuilder.myAudioURL(userid), RequestBuilder.getMyAudio(offset), new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                VKActionsAudio.onFail(e.toString());
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try {
                    if(response.isSuccessful()) {
                        ArrayList<Audio> audioList = new ArrayList();
                        //JSONObject obj = new JSONObject(response.body().string());
                        //String pageName = obj.getJSONObject("pageInfo").getString("pageName");
                        JSONArray arrResponse = new JSONArray(response.body().string());
                        // 3-й элемент не всегда присутсвует ?
                        JSONObject audioElem = arrResponse.getJSONArray(3).getJSONObject(0);
                        JSONObject objResponse = audioElem;//new JSONObject(audioElem);

                        Iterator<?> keys = objResponse.keys();

                        while (keys.hasNext()) {
                            String key = (String) keys.next();
                            if (objResponse.get(key) instanceof JSONArray) {
                                JSONArray arrAudio = objResponse.getJSONArray(key);
                                System.out.println(arrAudio.get(3) + " - " + arrAudio.get(4) + "::" + arrAudio.get(2) + "::" + arrAudio.get(5));
                                if(!arrAudio.getString(3).equals("")) {
                                    //try {
                                    Audio audio = new Audio();
                                    audio.ARTIST = arrAudio.get(3).toString();
                                    audio.TITLE = arrAudio.get(4).toString();
                                    audio.SOURCE = arrAudio.get(2).toString();
                                    // 5-й элемент не всегда присутсвует ?
                                    audio.DURATION = arrAudio.getInt(5);
                                    audio.SOURCE_IMAGE = arrAudio.get(8).toString();

                                   // new Util.GetPreviewImage(audio).execute(audio.SOURCE_IMAGE);

                                    Matcher m = PatternRegex.exec(PatternRegex.AudioImage, audio.SOURCE_IMAGE);
                                    if(m.find()){
                                        String ImagePreviewURL = m.group(1);
                                        try {
                                            URL newurl = new URL(ImagePreviewURL);
                                            audio.IMAGE = BitmapFactory.decodeStream(newurl.openConnection().getInputStream());
                                        }catch (IOException e) {

                                        }

                                    }

                                    /*AudioStorage audioStorage = new AudioStorage(
                                            arrAudio.get(2).toString(),
                                            arrAudio.get(3).toString(),
                                            arrAudio.get(4).toString(),
                                            arrAudio.get(5).toString()
                                    );*/

                                    audioList.add(audio);
                                    //}catch (Exception e){
                                    //    Log.e("MY MYSIC", e.toString());
                                    //VKActionsAudio.onError(e);
                                    //}
                                }
                            }
                        }
                        VKActionsAudio.onSuccess(audioList);
                    } else {
                        VKActionsAudio.onFail("TIME OUT");
                    }
                } catch (JSONException e) {
                    VKActionsAudio.onError(e);
                }
            }
        });
    }


    public void TopAudio(int offset,  final VKActionsAudio VKActionsAudio) throws IOException {
        HTTPRequest.POST(client, RequestBuilder.topAudioURL(offset), RequestBuilder.getTopAudio(), new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                VKActionsAudio.onFail(e.toString());
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String jsonResponse = response.body().string();
                try {
                    JSONArray arrResponse = (new JSONArray(jsonResponse)).getJSONArray(3);
                    String res = arrResponse.getString(0);



                    Matcher matchArtist = PatternRegex.exec(PatternRegex.AudioArtist, res);
                    Matcher matchTitle = PatternRegex.exec(PatternRegex.AudioTitle, res);
                    Matcher matchMp3 = PatternRegex.exec(PatternRegex.AudioMP3, res);
                    Matcher matchDuration = PatternRegex.exec(PatternRegex.AudioDuration, res);
                    Matcher matchImage = PatternRegex.exec(PatternRegex.AudioImage, res);

                    ArrayList<Audio> audioList = new ArrayList<>();

                    while (
                            matchMp3.find() &&
                                    matchArtist.find() &&
                                    matchTitle.find() &&
                                    matchDuration.find()
                            ) {

                      /*  AudioStorage audioStorage = new AudioStorage(
                                matchMp3.group(1),
                                matchArtist.group(1),
                                matchTitle.group(1),
                                matchDuration.group(1)
                        );*/
                        Audio audio = new Audio();
                        audio.ARTIST    = matchArtist.group(1);
                        audio.TITLE     = matchTitle.group(1);
                        audio.SOURCE    = matchMp3.group(1);
                        audio.DURATION  = Integer.valueOf(matchDuration.group(1));


                        if(matchImage.find()){
                            String ImagePreviewURL = matchImage.group(1);
                            try {
                                URL newurl = new URL(ImagePreviewURL);
                                audio.IMAGE = BitmapFactory.decodeStream(newurl.openConnection().getInputStream());
                            }catch (IOException e) {
                                Log.d("HTTPWRAPPER", e.toString());
                            }

                        }

                        audioList.add(audio);
                    }

                    VKActionsAudio.onSuccess(audioList);
                }catch (Exception e){
                    VKActionsAudio.onError(e);
                }
            }
        });
    }


    public void SerachAudio(int offset, String query,  final VKActionsAudio VKActionsAudio) throws IOException {
        HTTPRequest.POST(client, RequestBuilder.SearchAudioURL(offset, query), RequestBuilder.getSearchAudio(), new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
               VKActionsAudio.onFail(e.toString());
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String jsonResponse = response.body().string();
                try {

                    JSONArray arrResponse = new JSONArray(jsonResponse);
                    if(arrResponse.get(3) instanceof JSONArray){
                        arrResponse = arrResponse.getJSONArray(3);
                    }else{
//                        String resString = arrResponse.getString(3);
//                        if(resString.equals("Подтверждение")){
                            throw new IllegalAccessException("request captcha");
//                        }
                    }
                    String res = arrResponse.getString(0);
                    if(res.isEmpty())
                        res = arrResponse.getString(1);
//                    JSONArray arrResponse = (new JSONArray(jsonResponse)).getJSONArray(3);
//                    String res = arrResponse.getString(0);



                    Matcher matchArtist = PatternRegex.exec(PatternRegex.AudioArtist, res);
                    Matcher matchTitle = PatternRegex.exec(PatternRegex.AudioTitle, res);
                    Matcher matchMp3 = PatternRegex.exec(PatternRegex.AudioMP3, res);
                    Matcher matchDuration = PatternRegex.exec(PatternRegex.AudioDuration, res);
                    Matcher matchImage = PatternRegex.exec(PatternRegex.AudioImage, res);

                    ArrayList<Audio> audioList = new ArrayList<>();

                    while (
                            matchMp3.find() &&
                                    matchArtist.find() &&
                                    matchTitle.find() &&
                                    matchDuration.find()
                            ) {


                        Audio audio = new Audio();
                        audio.ARTIST    = matchArtist.group(1).replace("<em class=\"found\">", "").replace("</em>", "");
                        audio.TITLE     = matchTitle.group(1).replace("<em class=\"found\">", "").replace("</em>", "");
                        audio.SOURCE    = matchMp3.group(1);
                        audio.DURATION  = Integer.valueOf(matchDuration.group(1));

                        if(matchImage.find()){
                            String ImagePreviewURL = matchImage.group(1);
                            try {
                                URL newurl = new URL(ImagePreviewURL);
                                audio.IMAGE = BitmapFactory.decodeStream(newurl.openConnection().getInputStream());
                            }catch (IOException e) {
                                Log.d("HTTPWRAPPER", e.toString());
                            }

                        }

                       /* AudioStorage audioStorage = new AudioStorage(
                                matchMp3.group(1),
                                matchArtist.group(1).replace("<em class=\"found\">", "").replace("</em>", ""),
                                matchTitle.group(1).replace("<em class=\"found\">", "").replace("</em>", ""),
                                matchDuration.group(1)
                        );*/

                        audioList.add(audio);
                    }

                    VKActionsAudio.onSuccess(audioList);
                }catch (Exception e){
                    VKActionsAudio.onError(e);
                }
            }
        });
    }
}
