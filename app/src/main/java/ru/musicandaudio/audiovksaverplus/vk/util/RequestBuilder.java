package ru.musicandaudio.audiovksaverplus.vk.util;

import okhttp3.FormBody;
import okhttp3.HttpUrl;
import okhttp3.RequestBody;

/**
 * Created by ПК on 09.12.2016.
 */
public class RequestBuilder {
    /*
     #
     # Request to logIN
     # - HttpURL
     # - RequestBody
     #
     */

    public static HttpUrl buildAuthURL() {
        return new HttpUrl.Builder()
                .scheme("https") //http
                .host("m.vk.com")
                        //.addQueryParameter("param1", "value1") //add query parameters to the URL
                        //.addEncodedQueryParameter("encodedName", "encodedValue")//add encoded query parameters to the URL
                .build();
        /**
         * The return URL:
         *  https://www.somehostname.com/pathSegment?param1=value1&encodedName=encodedValue
         */
    }

    public static RequestBody AuthInVk(String login, String password) {
        return new FormBody.Builder()
                .add("email", login)
                .add("pass", password)
                .build();
    }


    /*
     #
     # check code on auth
     # - HttpURL
     # - RequestBody
     #
     */

    public static RequestBody AuthCheckCode(String code){
        return new FormBody.Builder()
                .add("code", code)
                .add("_ajax", "1")
                .add("remember", "1")
                .build();
    }

    /*
     #
     # Get my audios
     # - HttpURL
     # - RequestBody
     #
     */
    public static HttpUrl myAudioURL(String userid) {
        return new HttpUrl.Builder()
                .scheme("https") //http
                .host("m.vk.com")
                .addPathSegment("audios" + userid)
                        //.addQueryParameter("param1", "value1") //add query parameters to the URL
                        //.addEncodedQueryParameter("encodedName", "encodedValue")//add encoded query parameters to the URL
                .build();

    }

    public static RequestBody getMyAudio(int offset) {
        return new FormBody.Builder()
                .add("_ajax", "1")
                .add("offset", Integer.toString(offset))
                .build();
    }

    /*
     #
     # Get top audios
     # - HttpURL
     # - RequestBody
     #
     */
    public static HttpUrl topAudioURL(int offset) {
        return new HttpUrl.Builder()
                .scheme("https") //http
                .host("m.vk.com")
                .addPathSegment("audio")
                .addQueryParameter("act", "popular")
                .addQueryParameter("offset", Integer.toString(offset))
                        //.addQueryParameter("param1", "value1") //add query parameters to the URL
                        //.addEncodedQueryParameter("encodedName", "encodedValue")//add encoded query parameters to the URL
                .build();

    }

    public static RequestBody getTopAudio() {
        return new FormBody.Builder()
                .add("_ajax", "1")
                .build();
    }

    /*
     #
     # Serach audios
     # - HttpURL
     # - RequestBody
     #
     */

    public static HttpUrl SearchAudioURL(int offset, String query) {
        return new HttpUrl.Builder()
                .scheme("https") //http
                .host("m.vk.com")
                .addPathSegment("audio")
                .addQueryParameter("act", "search")
                .addQueryParameter("q", query)
                .addQueryParameter("_ajax", "1")
                .addQueryParameter("offset", Integer.toString(offset))
                        //.addQueryParameter("param1", "value1") //add query parameters to the URL
                        //.addEncodedQueryParameter("encodedName", "encodedValue")//add encoded query parameters to the URL
                .build();

    }

    public static RequestBody getSearchAudio() {
        return new FormBody.Builder()
                .add("_ajax", "1")
                .build();
    }


    /*
    * GET NAME & UID of user via friend page
     */
    public static HttpUrl buildFriendsURL() {
        return new HttpUrl.Builder()
                .scheme("https") //http
                .host("vk.com")
                .addPathSegment("friends")
                //.addQueryParameter("param1", "value1") //add query parameters to the URL
                //.addEncodedQueryParameter("encodedName", "encodedValue")//add encoded query parameters to the URL
                .build();
    }

}
