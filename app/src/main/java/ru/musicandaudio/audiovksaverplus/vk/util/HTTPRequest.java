package ru.musicandaudio.audiovksaverplus.vk.util;

import java.io.IOException;

import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

/**
 * Created by ПК on 09.12.2016.
 */

public final class HTTPRequest {
    //GET network request
    public static void GET(OkHttpClient client, HttpUrl url, Callback cb) throws IOException {
        Request request = new Request.Builder()
                .header("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.167 Safari/537.36")
                .addHeader("Cookie", "remixlang=3") //change lagg on eng
                .url(url)
                .build();
        /*Response response = client.newCall(request).execute();
        return response.body().string();*/

        client.newCall(request).enqueue(cb);
    }

    //POST network request
    public static void POST(OkHttpClient client, HttpUrl url, RequestBody body, Callback cb) throws IOException {
        Request request = new Request.Builder()
                .header("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.167 Safari/537.36")
                .header("x-requested-with", "XMLHttpRequest")
                .url(url)
                .post(body)
                .build();
        client.newCall(request).enqueue(cb);
    }
}
