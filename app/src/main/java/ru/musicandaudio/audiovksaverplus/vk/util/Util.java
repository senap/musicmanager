package ru.musicandaudio.audiovksaverplus.vk.util;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;

import com.evgenii.jsevaluator.JsEvaluator;
import com.evgenii.jsevaluator.interfaces.JsCallback;
import com.yandex.metrica.YandexMetrica;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import ru.musicandaudio.audiovksaverplus.vk.model.Audio;
import ru.musicandaudio.audiovksaverplus.vk.model.User;


/**
 * Created by PK on 06.04.2018.
 */

public class Util {

    public interface DecodeAudio {
        void onDecode(String resultLink);
    }

    public interface UPrepareAudio {
        void onResult(Audio audio);
    }

    public static void decodeAudio(Context ctx, String audioLink, final DecodeAudio callback){
        JsEvaluator jsEvaluator = new JsEvaluator(ctx);
        if(PatternRegex.JSCODE.length() < 1)
            PatternRegex.loadJSCode(ctx);
        String jsEval = PatternRegex.JSCODE.replace("%MP3FILE%", audioLink);
        jsEval = jsEval.replace("%VKID%", User.ID);
        jsEvaluator.evaluate(jsEval, new JsCallback() {
            @Override
            public void onResult(String result)  {
                try {
                    callback.onDecode(result);
                }catch (Exception e){
                    YandexMetrica.reportError("Util: decodeAudio", e);
                }

            }

            @Override
            public void onError(String errorMessage) {
                YandexMetrica.reportError("Player: getAudio", new Throwable(errorMessage));
            }
        });
    }

    public static  boolean requestStoragePermission(final Context ctx) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ctx.checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {

                AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
                builder.setTitle("Внимание!")
                        .setMessage("Для работы приложения, необходимо разрешить доступ к фото, мультимедиа и файлам на вашем устройстве.")
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        ActivityCompat.requestPermissions((Activity)ctx, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                                    }
                                });
                AlertDialog alert = builder.create();
                alert.show();

                return false;
            }
        }
        else {
            return true;
        }
    }

    public static boolean requestPhoneStatePermission(final Context ctx){
        if (Build.VERSION.SDK_INT >= 23) {
            if (ctx.checkSelfPermission(android.Manifest.permission.READ_PHONE_STATE)
                    == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                ActivityCompat.requestPermissions((Activity) ctx, new String[]{Manifest.permission.READ_PHONE_STATE}, 1);
                return false;
            }
        }else{
            return false;
        }
    }

    public static void saveAudio(final Context ctx, final Audio audio){
        requestStoragePermission(ctx);
        try{
            if(audio.SOURCE_DECODED != null){
                download(ctx, audio);
            } else {
                decodeAudio(ctx, audio.SOURCE, new DecodeAudio() {
                    @Override
                    public void onDecode(String resultLink) {
                        audio.SOURCE_DECODED = resultLink;
                        download(ctx, audio);
                    }
                });
            }
        }catch (Exception e){
            YandexMetrica.reportError("Util: saveAudio", e);
        }
    }

    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            // Log exception
            return null;
        }
    }

    private static void download(Context ctx, Audio audio){
        long downloadReference;
        Snackbar mSnackbar;
        DownloadManager downloadManager;
        Uri uri = Uri
                .parse(audio.SOURCE_DECODED);
        String resultFileName = prepareName(audio.ARTIST+" - "+audio.TITLE+".mp3");

        downloadManager = (DownloadManager)ctx.getSystemService(ctx.DOWNLOAD_SERVICE);
        DownloadManager.Request request = new DownloadManager.Request(uri);

        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        //Setting title of request
        request.setTitle(resultFileName);
        //Setting description of request
        Resources appR = ctx.getResources();
        CharSequence app_name = appR.getText(appR.getIdentifier("app_name",
                "string", ctx.getPackageName()));

        request.setDescription(app_name);
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, resultFileName);

        //Enqueue download and save into referenceId
        downloadReference = downloadManager.enqueue(request);

    }

    private static String prepareName(String name){
        char[] symbols = {'|','/', '>', '<', '&', '#', ';', ':'};
        for(int i = 0; i<symbols.length; i++) {
            name = name.replace(symbols[i], ' ');
        }

        return name;
    }

//    public static class GetPreviewImage extends AsyncTask<String, Void, Bitmap> {
//        Audio audio;
//        public GetPreviewImage(Audio audio) {
//           this.audio = audio;
//        }
//
//        protected Bitmap doInBackground(String... source) {
//            Matcher m = PatternRegex.exec(PatternRegex.AudioImage, source[0]);
//            Bitmap bitmap = null;
//            if(m.find()){
//                String ImagePreviewURL = m.group(1);
//                try {
//                    URL newurl = new URL(ImagePreviewURL);
//                    bitmap = BitmapFactory.decodeStream(newurl.openConnection().getInputStream());
//                }catch (IOException e) {
//
//                }
//
//            }
//            return bitmap;
//        }
//
//        protected void onPostExecute(Bitmap result) {
//           audio.IMAGE = result;
//        }
//    }


//    public static class PrepareAudio extends AsyncTask<Audio, Void, Audio> {
//
//        UPrepareAudio AudioCallBack;
//        public PrepareAudio(UPrepareAudio  AudioCallBack){
//            this.AudioCallBack = AudioCallBack;
//        }
//
//        protected Audio doInBackground(Audio... audios) {
//            final Audio audio = audios[0];
//            Matcher m = PatternRegex.exec(PatternRegex.AudioImage, audio.SOURCE_IMAGE);
//            if(m.find()){
//                String ImagePreviewURL = m.group(1);
//                try {
//                    URL newurl = new URL(ImagePreviewURL);
//                    audio.IMAGE = BitmapFactory.decodeStream(newurl.openConnection().getInputStream());
//                }catch (IOException e) {
//
//                }
//            }
//
//            JsEvaluator jsEvaluator = new JsEvaluator(ctx);
//            if(PatternRegex.JSCODE.length() < 1)
//                PatternRegex.loadJSCode(ctx);
//            String jsEval = PatternRegex.JSCODE.replace("%MP3FILE%", audio.SOURCE);
//            jsEval = jsEval.replace("%VKID%", User.ID);
//            jsEvaluator.evaluate(jsEval, new JsCallback() {
//                @Override
//                public void onResult(String result)  {
//                    try {
//                        if(!result.equals("undefined")) {
//                            audio.SOURCE_DECODED = result;
//
//                            MediaMetadataRetriever retriever = new MediaMetadataRetriever();
//                            retriever.setDataSource(audio.SOURCE_DECODED, new HashMap<String, String>());
//
//
//                            audio.BITRATE = retriever.extractMetadata(METADATA_KEY_BITRATE);
//                            new Util.GetPreviewImage(audio).execute(audio.SOURCE_IMAGE);
//
//                            YandexMetrica.reportEvent("Player setAudio: play audio after decodeAudioURl");
//                        }else{
//                            description.setState(Player.Description.STATE_ERROR_DECODE);
//                            emitListenerChange(audio, description);
//                        }
//                    }catch (Exception e){
//                        YandexMetrica.reportError("Util: decodeAudio", e);
//                    }
//
//                }
//
//                @Override
//                public void onError(String errorMessage) {
//                    YandexMetrica.reportError("Player: getAudio", new Throwable(errorMessage));
//                }
//            });
//
//            return audio;
//        }
//
//        protected void onPostExecute(Audio audio) {
//            AudioCallBack.onResult(audio);
//        }
//    }

}
